from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import (
    DetailView, TemplateView, UpdateView, CreateView
)

from membership.forms import SignupForm, AddressFormSet, PhoneNumberFormSet
from membership.models import Family

from pack_calendar.models import Event

from .models import StaticPage, DynamicPage


class AboutPageView(TemplateView):
    template_name = 'pages/about_page.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            context['page_content'] = StaticPage.objects.get(
                page=StaticPage.ABOUT
            )
        except StaticPage.DoesNotExist:
            context['page_content'] = None

        return context


class HomePageView(TemplateView):
    template_name = 'pages/home_page.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['event_list'] = Event.objects.filter(
            start__lte=timezone.now() + timezone.timedelta(weeks=1)
        ).filter(
            start__gte=timezone.now() - timezone.timedelta(hours=8)
        ).order_by('start')
        try:
            context['page_content'] = StaticPage.objects.get(
                page=StaticPage.HOME
            )
        except StaticPage.DoesNotExist:
            context['page_content'] = None
        return context


class HistoryPageView(TemplateView):
    template_name = 'pages/history_page.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['page_content'] = StaticPage.objects.get(
                page=StaticPage.HISTORY
            )
        except StaticPage.DoesNotExist:
            context['page_content'] = None
        return context


class SignUpPageView(CreateView):
    form_class = SignupForm
    success_url = reverse_lazy('login')
    template_name = 'pages/signup_page.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['address_formset'] = AddressFormSet(
                self.request.POST
            )
            context['phonenumber_formset'] = PhoneNumberFormSet(
                self.request.POST
            )
        else:
            context['address_formset'] = AddressFormSet()
            context['phonenumber_formset'] = PhoneNumberFormSet()
        try:
            context['page_content'] = StaticPage.objects.get(
                page=StaticPage.SIGNUP
            )
        except StaticPage.DoesNotExist:
            context['page_content'] = None
        return context

    def form_valid(self, form):
        context = self.get_context_data(form=form)
        address_formset = context['address_formset']
        phonenumber_formset = context['phonenumber_formset']
        if address_formset.is_valid() and phonenumber_formset.is_valid():
            self.object = form.save()
            form.instance.family = Family.objects.create()
            address_formset.instance = self.object
            address_formset.save()
            phonenumber_formset.instance = self.object
            phonenumber_formset.save()
        else:
            return super().form_invalid(form)
        return super().form_valid(form)


class DynamicPageView(DetailView):
    model = DynamicPage
    context_object_name = 'page_content'


class DynamicPageUpdateView(UpdateView):
    model = DynamicPage
    context_object_name = 'page_content'
    fields = '__all__'
